var _ = require('lodash');

function getCaller() {
  var stack = getStack()

  // Remove superfluous function calls on stack
  stack.shift() // getCaller --> getStack
  stack.shift() // omfg --> getCaller
  stack.shift() // Log -> getLogger

  // Return caller's caller
  return stack[1].receiver
}

function getStack() {
  // Save original Error.prepareStackTrace
  var origPrepareStackTrace = Error.prepareStackTrace

  // Override with function that just returns `stack`
  Error.prepareStackTrace = function (_, stack) {
    return stack
  }

  // Create a new `Error`, which automatically gets `stack`
  var err = new Error()

  // Evaluate `err.stack`, which calls our new `Error.prepareStackTrace`
  var stack = err.stack

  // Restore original `Error.prepareStackTrace`
  Error.prepareStackTrace = origPrepareStackTrace

  // Remove superfluous function call on stack
  stack.shift() // getStack --> Error

  return stack
}

function isInMaskList(maskList, moduleName) {
	var isInList = false;

	_.each(maskList.string, function(mask) {
		if (mask == moduleName) {
			isInList = true;
			return false;
		}
	});

	if (isInList) return isInList;

	_.each(maskList.regexp, function(mask) {
		if (moduleName.match(mask)) {
			isInList = true;
			return false;
		}
	});

	return isInList;
}

var Logger = function(moduleName) {
	if (!moduleName) {
		moduleName = getCaller().filename;
		moduleName = moduleName.replace(process.cwd(), '');
	}

	if (isInMaskList(Logger.errorList, moduleName)) {
		this.logLevel = 1;
	}
	if (isInMaskList(Logger.warnList, moduleName)) {
		this.logLevel = 2;
	}
	if (isInMaskList(Logger.debugList, moduleName)) {
		this.logLevel = 3;
	}
	if (isInMaskList(Logger.infoList, moduleName)) {
		this.logLevel = 4;
	}

	this.moduleName = moduleName;

	if (Logger.useShurtModulePath) {
		var prefix = '';
		if (/^\/node_modules/.exec(this.moduleName)) {
			prefix = this.moduleName.split('/')[2];
			prefix += ': ';
		}
		prefix += this.moduleName.split('/').reverse()[0];

		this.moduleName = prefix;
	}
};

var config = global.config || {};
var logConf = config.log || {};

function prepareLists(src) {
	var dst = {};
	dst.string = [];
	dst.regexp = [];
	_.each(src, function(mask) {
		if (_.isString(mask)) {
			dst.string.push(mask);
		}
		if (_.isRegExp(mask)) {
			dst.regexp.push(mask);
		}
	});

	return dst;
}

Logger.infoList = prepareLists(logConf.info || []);
Logger.debugList = prepareLists(logConf.debug || []);
Logger.warnList = prepareLists(logConf.warn || []);
Logger.errorList = prepareLists(logConf.error || [/.*/]);

Logger.useShurtModulePath = logConf.useShurtModulePath;

Logger.prototype = {
	moduleName: null,
	logLevel: 0,

	getPrefix: function() {
		return '[' + this.moduleName + ']';
	},

	info: function() {
		if (4 <= this.logLevel) {
			var args = Array.prototype.slice.call(arguments, 0);
			args.splice(0, 0, 'INFO:', this.getPrefix());
			console.log.apply(this, args);
		}
	},
	debug: function() {
		if (3 <= this.logLevel) {
			var args = Array.prototype.slice.call(arguments, 0);
			args.splice(0, 0, 'DEBUG:', this.getPrefix());
			console.log.apply(this, args);
		}
	},
	warn: function() {
		if (2 <= this.logLevel) {
			var args = Array.prototype.slice.call(arguments, 0);
			args.splice(0, 0, 'WARN:', this.getPrefix());
			console.log.apply(this, args);
		}
	},
	error: function() {
		if (1 <= this.logLevel) {
			var args = Array.prototype.slice.call(arguments, 0);
			args.splice(0, 0, 'ERROR:', this.getPrefix());
			console.log.apply(this, args);
		}
	}
};

var Log = {
	getLogger: function(moduleName) {
		return new Logger(moduleName);
	}
};

global.Log = Log;
